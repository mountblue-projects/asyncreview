const fetch = require("node-fetch");

async function fetchUsers() {
  return new Promise(async (resolve, reject) => {
    const userDataPromises = await fetch("http://localhost:3000/users");
    const userData = await userDataPromises.json();
    resolve(userData);
  })
}

async function fetchTodos(userId) {
  return new Promise(async (resolve, reject) => {
    const todoPromises = await fetch(`http://localhost:3000/todos?user_id=${userId}`);
    const todoData = await todoPromises.json();
    resolve(todoData);
  })
}

function fetch5todos(currentId) {
  return new Promise((resolve, reject) => {
    let ans = [];
    for (let i = currentId; i < currentId + 5; i++) {
      const todo = fetchTodos(i);
      ans.push(todo);
    }
    console.log("fetched 5 files ...");
    resolve(Promise.all(ans));
  })
}


function timeOut() {
  return new Promise(resolve => setTimeout(resolve, 1000))
}

async function afterOneSec(curr, ans) {
  await timeOut();
  let current5usersData = await (fetch5todos(curr));
  return current5usersData;
}


async function main() {
  try {
    const usersData = await fetchUsers();
    console.log(usersData);

    // 2. 

    let ans = [];
    for (let i = 0; i < 3; i++) {
      let curr = (await afterOneSec((i * 5) + 1));
      ans.push(curr);
    }
    // console.log(ans);
    let usersToDoData = [];

    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 5; j++) {
        usersToDoData.push(ans[i][j]);
      }
    }

    let final = [];
    for (let currentUser of usersToDoData) {
      let currObj = {};
      for (let currentToDo in currentUser) {
        for (let keys of currentUser[currentToDo]) {
          if (currObj[((keys["id"]).split("-"))[1]] === undefined) {
            currObj[((keys["id"]).split("-"))[1]] = 0;
          }
          if (keys["isCompleted"] == true) {
            currObj[((keys["id"]).split("-"))[1]] += 1;
          }
        }
      }
      final.push(currObj);
    }

    let finalFormatedAns = []

    for (const currUsr of final) {
      currentUserObject = {
        id: "",
        name: "",
        numTodosCompleted: 0
      }
      for (const key in currUsr) {
        currentUserObject["id"] = key;
        currentUserObject["name"] = `User ${key}`;
      }
      for (const key in currUsr) {
        currentUserObject["numTodosCompleted"] = currUsr[key];
      }

      finalFormatedAns.push(currentUserObject);
    }
    console.log(finalFormatedAns);

  } catch (error) {
    throw error;
  }




}
main();


// write your code here
